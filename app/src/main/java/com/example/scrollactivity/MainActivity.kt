package com.example.scrollactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    private fun init() {
        login.setOnClickListener{
            signIn()
        }
    }
    private fun signIn() {
        if(email.text.toString().isNotEmpty() && password.text.toString().isNotEmpty())
        {
            openActivity()
        }
    }
    private fun openActivity() {
        val intent = Intent(this, secondActivity::class.java)
        startActivity(intent)
    }
}